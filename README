RFDump V1.6

Features
-------------------------------------------------------------------------------
RFDump is a tool to detect RFID-Tags and show their meta information:
Tag ID, Tag Type, manufacturer etc. The user data memory of a tag can
be displayed and modified using either a Hex or an ASCII editor. Tag
contents can be stored and loaded using a specific XML fomrat. This
effectively allows to copy data from one tag to another. In addition,
the integrated cookie feature demonstrates how easy it is for a
company to abuse RFID technology to spy on their customers.  RFDump
works with the ACG Multi-Tag Reader or similar card reader hardware.


Installation
-------------------------------------------------------------------------------
RFDump communicates with the RFID Reader in ASCII mode through a
serial port (9600 baud, 8N1). The software has been tested with the
ACG Multi-Tag Reader but should work with similar hardware as
well. Before starting RFDump connect your reader hardware to the
machine running RFDump (either directly through a serial port or via a
PCMCIA card adapter).

Install the software by extracting the installation file rfdump-1.6.tar.gz

$ gunzip rfdump-1.6.tar.gz

$ tar -xvf rfdump-1.6.tar

Change into the install directory and start the build process:

$ cd rfdump-1.6

$ ./configure

$ make

$ make install

Now you are ready to start RFDump:

$ ./rfdump

then set your serial port in the preferences. You can also use a TCP
stream to your reader with a "hostname:port" syntax (for example
"localhost:7343").


User Guide (Gtk Version)
-------------------------------------------------------------------------------
After starting RFDump you will see the main screen. Scanning for tags
will start automatically. After starting it is recommended to wait a
few seconds before putting the first tag under the reader so the
hardware can be initialized.

When a tag is placed under the reader its type and ID will be
displayed.  Immediately after RFDump will start scanning the tag's
memory (address 0x00-0xFF) and display it in Hex- and
ASCII-representation in a table view. Each table cell represents a
memory page. Reading the entire memory may take some time. Usually a
flashing light at the reader will indicate a reading operation in
progress.  The reading process can be aborted by removing the tag from
underneath the reader at any time. Once the entire memory is read
RFDump will continue to scan for new tags but not re-read the current
tag.

A memory page that has been read may be selected and modified. RFDump
makes sure that pages have the correct length (8 or 16 bytes
respectively, depending on the tag type) by padding zeros or
truncating extra digits. When deselecting a modified table cell RFDump
tries to write the modified page immediately to the tag. Note, that it
is possible to edit the ASCII as well as the Hex data.  The
corresponding fields are updated automatically.

Note: Some tags have less than 0xFF pages, others map the same memory
area to higher address spaces several times.

Checking the "Activate" check box enables the cookie feature. This
feature will place a configurable cookie ID (default "DEADFACE") into
the third last memory page and initialize a counter in the following
memory page. After setting the cookie for the first time the counter
will automatically be incremented by one each time the tag is scanned.


Setup ACG readers
-------------------------------------------------------------------------------
If RFDump can't detect your reader you may want to setup your reader
with the Windows setup tool from ACG. Download
http://acg-id.aaitg.com/fileadmin/acg_id/products/readers/Zip/ReaderUtility_v3.2.0.zip
or look at the ACG page http://acg-id.aaitg.com/index.php?id=156 for
newer versions.

The ACG readers need to be setup in "ASCII mode" with 9600 baud. Be
aware that the ACG PC/SC windows driver always reconfigures any found
ACG readers to binary mode, so RFDump can't detect them any longer.


Working with the VMWare image
-------------------------------------------------------------------------------
If you have a USB reader it should be automatically bound to the
running VMWare if you plug in the reader after the VMWare Linux has
started. The reader will be available as /dev/ttyUSB0.

If you have a CF reader you have to manually map the Windows COM port
to a Linux tty by configuring your VMWare
player/workstation/server. When using the ACG setup tool you can
determine the Windows COM port. When using VMWare player you have to
edit the .vmx file and manually map the COM port to tty0..tty3. If you
have a graphical configuration dialog this is a little bit easier. In
the VMWare Linux the reader is then accessible as
/dev/ttyS0.../dev/ttyS3.


Contact
-------------------------------------------------------------------------------
RFDump Website: http://www.rfdump.org/

Questions and suggestions should be posted to the RFDump maillist. You
can subscribe via http://lists.dn-systems.net/mailman/listinfo/rfdump
or by sending a mail to rfdump-request@lists.ezisp.de with "subscribe"
in the body.
