/* Copyright (c) 2005-2008 DN-Systems GmbH http://rfdump.org

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA */

/* $Header: /cvs/rfdump/src/main.h,v 1.19 2008/02/01 19:45:36 dirk Exp $ */

#ifndef MAIN__H
#define MAIN__H

#include <gtk/gtk.h>
#include <glade/glade.h>
#include "rfid.h"

char dec2hex(const int i);
int getCharValue(char c);

void stop_scan();
void start_scan();
void statusbar_set(char *str);
void poll_gtk();
void read_memory_tick(int);
void write_memory_tick(int);
void setup_table();
void error_dialog(char *message);
void info_dialog(char *message);

void on_set_to_defaults1_activate(GtkMenuItem *menuitem, gpointer user_data);
void writePreferences();

typedef struct {
  const char *readerdevice;
  const char *baudrate;
  int binarymode;
} init_reader_arg;

gint init_reader(gpointer data);

extern int scan_run;
extern int use_cookies;
extern gchar *cookie_id;
extern struct RFIDTag tag;
extern int fd;
extern GtkWidget *windowRFDump, *windowAbout, *windowPreferences, *windowMiFareCrack;
extern GtkWidget *keya, *keyb, *entryTagID, *entryManufacturer, *entryTagType;
extern GladeXML *glade_xml;
extern GKeyFile *prefs;
extern GtkTreeModel *model;

extern char *PREFS_FN;
#define PREFS_GROUP "rfdump"
#define PREFS_COOKIE "cookie_id"
#define PREFS_COOKIE_ACTIVE "cookie_active"

#define PREFS_ACG_SERIALPORT "acg_serialport"
#define PREFS_ACG_BAUDRATE "acg_baudrate"
#define PREFS_ACG_BINARYMODE "acg_binarymode"

#endif
