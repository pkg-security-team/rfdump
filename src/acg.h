/* Copyright (c) 2005-2007 DN-Systems GmbH http://rfdump.org

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA */

/* $Header: /cvs/rfdump/src/acg.h,v 1.9 2007/10/31 21:59:27 dirk Exp $ */

#ifndef ACG__H
#define ACG__H

int acg_printinfo_iso_0_9(int fd);
int acg_setup_iso_0_9(int fd);

int acg_printinfo_dual_2_2(int fd);
int acg_setup_dual_2_2(int fd);

int acg_printinfo_multiiso_1_0(int fd);
int acg_setup_multiiso_1_0(int fd);

int acg_printinfo_lfx_1_0(int fd);
int acg_setup_lfx_1_0(int fd);

#endif
