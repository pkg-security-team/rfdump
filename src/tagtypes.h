/* Copyright (c) 2005-2007 DN-Systems GmbH http://rfdump.org

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA */

/* $Header: /cvs/rfdump/src/tagtypes.h,v 1.6 2007/10/31 21:59:27 dirk Exp $ */

#ifndef TAGTYPES__H
#define TAGTYPES__H

struct RFIDTagType {
   char tagTypeID[255];
   char tagTypeName[255];
   char tagManufacturerName[255];
   int memSize;
   int pageSize;
};

int readTagTypes(struct RFIDTagType **tagTypes, const int max);

int getTagType(char *id, struct RFIDTagType **tagTypes);

#endif
