/* Copyright (c) 2006 Dirk Jagdmann <doj@cubic.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA */

/* $Header: /cvs/rfdump/src/tools.h,v 1.4 2006/07/27 10:46:11 dirk Exp $ */

#ifndef TOOLS__H
#define TOOLS__H

void error_dialog(char *message);

#define sizeof_array(x) (sizeof(x)/sizeof(x[0]))

#define SNPRINTF(buf, size, format, a...) \
do { \
  int _i_=snprintf(buf, size, format, ##a); \
  if(_i_>=size) { \
    error_dialog("a snprintf() was truncated!");	\
  } \
} while(0)

#endif
