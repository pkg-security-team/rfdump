/* Copyright (c) 2005-2008 DN-Systems GmbH http://rfdump.org

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA */

/* $Header: /cvs/rfdump/src/rfid.h,v 1.23 2008/02/02 01:58:41 dirk Exp $ */

#ifndef RFID__H
#define RFID__H

#include <stdint.h>

#define RFID_SIZE 256
struct RFIDTag {
  char tagID[RFID_SIZE];
  char tagTypeName[RFID_SIZE];
  char tagManufacturerName[RFID_SIZE];
  int memSize;
  int pageSize;
  char* mem[RFID_SIZE];
  char* keya[RFID_SIZE];
  char* keyb[RFID_SIZE];
};
#undef RFID_SIZE

void tag_clear(struct RFIDTag *t);
void tag_copy(struct RFIDTag *dst, const struct RFIDTag *src);
void tag_copydata(struct RFIDTag *dst, const struct RFIDTag *src);
uint8_t tag_getbyte(const struct RFIDTag *t, const unsigned n);
int tag_setbyte(struct RFIDTag *t, const unsigned n, const uint8_t b);

/* RFID API constants */

extern char* RFID_CMD_READ_PAGE;
extern char* RFID_CMD_WRITE_PAGE;
extern char* RFID_CMD_LOGIN;

#define RFID_ERR_UNKNOWN_CMD '?'
#define RFID_ERR_COLLISION 'C'
#define RFID_ERR_GENERAL_FAILURE 'F'
#define RFID_ERR_INVALID_DATA 'I'
#define RFID_ERR_PAGE_LOCKED 'L'
#define RFID_ERR_NO_TAG 'N'
#define RFID_ERR_OPMODE_FAIL 'O'
#define RFID_ERR_OUT_OF_RANGE 'R'
#define RFID_ERR_WRITE_FAILURE 'U'

#define RFID_MSG_UNKNOWN_CMD "Unknown Command"
#define RFID_MSG_COLLISION "Collision of CRC/MAC Error"
#define RFID_MSG_GENERAL_FAILURE "General Failure"
#define RFID_MSG_INVALID_DATA "Invalid Data"
#define RFID_MSG_PAGE_LOCKED "Page Lock Failure"
#define RFID_MSG_NO_TAG "No Tag"
#define RFID_MSG_OPMODE_FAIL "Operation mode failure or file not selected"
#define RFID_MSG_OUT_OF_RANGE "Command parameter out of range"
#define RFID_MSG_WRITE_FAILURE "Write Failure"

#define RFID_BLANK ""
#define RFID_COOKIE_ID "DEADFACE"

/* RFID API */

extern int binary_mode;

int rfidConnectReader(const char *readerdevice, int baudrate, int binarymode_);
int rfidReset(int fd);
void serial_cleanup();

int rfidSelect(int fd, char *result, int result_size);
int rfidReadLine(int fd, char *result, int bufsize);

extern int (*rfidReadPage)(int fd, int pageNo, char *page, int pagesize);
extern int (*rfidWritePage)(int fd, int pageNo, char *page);

int rfidReadAllPages(int fd, struct RFIDTag *tag);
int rfidWriteAllPages(int fd, struct RFIDTag *tag);

int rfidInitCookie(int fd, struct RFIDTag *tag, char *cookieID);
int rfidIncrementCookie(int fd, struct RFIDTag *tag);

void rfidGetTagID(char *select, char *tagID);

int rfidWrite(int fd, char *cmd);

extern int (*rfidReaderSetup)(int fd);
extern int (*rfidReaderInfo)(int fd);

int search_baud_rate();

#endif
